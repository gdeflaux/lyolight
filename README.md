# LyoLight

LyoLamp is a smart desk lamp that is connected to the internet. It displays time and weather information for different locations. The entire project is documented in the [wiki](https://gitlab.com/gdeflaux/lyolight/-/wikis/Home).

![Front view](lamp-case/build-images/front.jpg)