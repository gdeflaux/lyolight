/*
 * This is a sample file containing all the information required
 * to generate the required 'private_keys_vars.h' file.
 * 
 * 1. Rename this file to 'private_keys_vars.h' 
 * 
 * 2. Update the API keys
 *  - OPENWEATHERMAP_API_KEY    API key for OpenWeatherMap (https://home.openweathermap.org/api_keys)
 *  - BLYNK_API_KEY             Obtained when creating your project in the Blynk application
 *  
 *  3. Add all the wifi networks you want Lyolight to scan and connect to.
 */

#define OPENWEATHERMAP_API_KEY  "fkf7f7afa8q9jfnf72nw9vndks8n" // Replace by your key
#define BLYNK_API_KEY           "fkf7f7afa8q9jfnf72nw9vndks8n" // Replace by your key

// Array of WifiNetwork objects that describes all the wifi networks you want Lyolight to scan and connect to.
WifiNetwork  wifi_networks[] = {
  { "My network Name (SSID)", "My Network Password" },
  { "home-network", "1234abcd" }
  //{ "My network Name", "My Network Password" }
  //{ "My network Name", "My Network Password" }
  //{ "My network Name", "My Network Password" }
 };

WifiNetworkManager  network_manager(2, wifi_networks); // Update the number of networks defined in 'wifi_networks'.
