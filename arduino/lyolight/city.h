#ifndef _CITY_H_
#define _CITY_H_

#include "shift_register.h"

/*
 * Data definition for a city
 * 
 * name:        Name of the city
 * time_api:    City identifier in WorldTimeApi API calls
 * weather_api: City identifier in the OpenWeatherMap  API calls
 * sr_bit:      LED identifier for the shit register (only one bit set to 1)
 *              Cities use the 4 bits on the right of the register (most significant bits)
 * 
 */
class City {
  public:
  City();
  City(String _name, String _time_api, String _weather_api,  byte _sr_bit) {
     name = _name;
     time_api = _time_api;
     weather_api = _weather_api;
     sr_bit = _sr_bit;
  }
  
  String  name;
  String  time_api;
  String  weather_api;
  byte    sr_bit;
};

/*
 * Support functions to manage city data and operations
 * 
 * current_city:  Currently selected city
 * n_cities:      Number of cities
 * cities:        Array of cities
 * sr:            Shift register
 * 
 * next_city():   Updates current_city
 * city_name():   Helper function that returns the 'name' of the current city
 * time_api():    Helper function that returns the 'time_api' of the current city
 * weather_api(): Helper function that returns the 'weather_api' of the current city
 * update_led():  Lights up the LED for the current city
 * 
 */
class CityManager {
  public:
  CityManager(int _n_cities, City *_cities, ShiftRegister *_sr) {
    current_city = 0;
    n_cities = _n_cities;
    cities = _cities;
    sr = _sr;
  }
  
  int           current_city;
  int           n_cities;
  City          *cities;
  ShiftRegister *sr;

  void  next_city() {
    current_city = (current_city == n_cities - 1) ? 0 : (current_city + 1);  
  }

  String  city_name() {
    return cities[current_city].name;
  }
  
  String  time_api() {
    return cities[current_city].time_api;
  }

  String  weather_api() {
    return cities[current_city].weather_api;
  }

  void update_led() {
    sr->update_register(cities[current_city].sr_bit, SR_LEFT_SIDE);
  }
};

#endif
