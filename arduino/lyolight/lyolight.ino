/*
 * Blynk Libraries
 */
#define BLYNK_PRINT Serial
#include <WiFiNINA.h>
#include <BlynkSimpleWiFiNINA.h>

/*
 * Display Libraries
 */
#include <MD_Parola.h>
#include <MD_MAX72xx.h>
#include <SPI.h>

/*
 * Temp/Hum Sensor Libraries
 */
#include "DHT.h"

/*
 * Other Libraries
 */
#include <Arduino_JSON.h> // Parse JSON easily
#include <Pushbutton.h> // Manage push button debounce
#include <TimeLib.h> // Manage time of the board easily

/*
 * LyoLigth Support Classes
 */
#include "shift_register.h"
#include "relay.h"
#include "city.h"
#include "weather.h"
#include "wifi_network.h"

/*
 * Private API keys and wifi networks
 * 
 * You have to add this file to the project. See instructions in 'private_keys_vars_sample.h'.
 */ 
#include "private_keys_vars.h"

/*
 * Wifi Networks
 */
bool found_known_network; // Have we found a know wifi network to connect to?

/*
 * Global variable declarations
 */

// Shift Register
ShiftRegister sr(3, 2, 8);

// DHT11 Sensor
#define DHT_PIN  7
DHT dht(DHT_PIN, DHT22);

/*
 * Led Matrix
 */
// Hardware configuration
#define DISP_HW_TYPE              MD_MAX72XX::FC16_HW // Hardware type corresponding to the MAX7219 display
#define DISP_DEVICES              4 // The display has 4 8x8 matrices
#define DISP_CLK_PIN              5
#define DISP_DATA_PIN             6
#define DISP_CS_PIN               4
#define DISP_SCROLL_SPEED         30 // Speed at which the text moves on the display. The lower the faster.

// Display functions
#define DISP_CONNECTED            0   // The board is connected to the Blynk server.
#define DISP_TIME                 1   // Show time with a transition
#define DISP_INSIDE_TEMP          2   // Show inside temperature
#define DISP_INSIDE_HUM           3   // Show inside humidity
#define DISP_WEATHER_DESC         4   // Show weather description 
#define DISP_WEATHER_TEMP         5   // Show temperature
#define DISP_WEATHER_TEMP_MIN_MAX 6   // Show temperature range
#define DISP_WEATHER_HUM          7   // Show humidity
#define DISP_INIT_TIME            8   // Update and show time after the board conntects to the Blynk Server
#define DISP_DEFAULT              9   // Show time without animation
#define DISP_UPDATE_WEATHER       10  // Triggers the weather webhook after the city is changed
#define DISP_TIME_NO_WEATHER      11  // Show time without updating the weather
#define DISP_NO_WIFI_FOUND        12  // Message that no known wifi was found
#define DISP_WIFI_NETWORKS        13  // Display list of known wifi networks
#define DISP_DO_NOTHING           99  // Do nothing
MD_Parola P = MD_Parola(DISP_HW_TYPE, DISP_DATA_PIN, DISP_CLK_PIN, DISP_CS_PIN, DISP_DEVICES);
int       display_func; // The currently selected display function
char      message[200] = ""; // The message displayed on the display

/*
 * Relay to control the bulb
 */
Relay bulb(9);

/*
 * Buttons
 */
#define BTN_PIN_CHANGE_CITY A6
#define BTN_PIN_WEATHER     A4
#define BTN_PIN_INSIDE_TEMP A5
#define BTN_PIN_BULB        A3
Pushbutton  btn_change_city(BTN_PIN_CHANGE_CITY);
Pushbutton  btn_weather(BTN_PIN_WEATHER);
Pushbutton  btn_inside_temp(BTN_PIN_INSIDE_TEMP);
Pushbutton  btn_bulb(BTN_PIN_BULB);

/*
 * City definitions & variables
 */
City cities[] = {
  { "New York", "/America/New_York", "New York,us", 0x10 },
  { "Buenos Aires", "/America/Argentina/Buenos_Aires", "Buenos Aires,ar", 0x20 },
  { "Dakar", "/Africa/Dakar", "Dakar,sn", 0x40 },
  { "Lyon", "/Europe/Paris", "Lyon,fr", 0x80 }
};
CityManager city_manager(4, cities, &sr);

/*
 * Weather definitions & variables
 */
WeatherManager  weather_manager(&sr);
bool display_the_weather = false; // If "false" the weather is not displayed after the webhook call

/*
 * Timers to send data back to the mobile application
 */
BlynkTimer  timer;

void setup()
{  
  Serial.begin(9600);

  
  found_known_network = network_manager.scanNetworks();
  
  // Initiate other hardware
  sr.begin();
  bulb.begin();
  dht.begin();

  // Initiate display
  P.begin();
  P.displayText(message, PA_CENTER, DISP_SCROLL_SPEED, 0, PA_PRINT);

  if (found_known_network) {
    // Start timers
    timer.setInterval(60000L, update_inside_temp_mobile); // 1min
    timer.setInterval(1800000L, update_weather_mobile); // 30min
  
    // Connect to Blynk Server on the network that has the strongest signal
    Blynk.begin(BLYNK_API_KEY, network_manager.best_network()->ssid.c_str(), network_manager.best_network()->password.c_str());
  } else {
    display_func = DISP_NO_WIFI_FOUND;
  }
}

void loop()
{
  static uint32_t  lastTime = 0;

  if (found_known_network) { // No need to call these functions if not connected to a known network
   Blynk.run();
   timer.run(); 
  }
  
  P.displayAnimate();

  // Controls what gets displayed.
  // Display functions are "chained".
  if (P.getZoneStatus(0)) {
    switch (display_func) {
      case DISP_NO_WIFI_FOUND:
        P.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_DOWN);
        P.setPause(2000);
        strcpy(message, "No Wifi network found :(");
        P.displayReset();
        display_func = DISP_WIFI_NETWORKS;
        break;

      case DISP_WIFI_NETWORKS:
        P.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_DOWN);
        P.setPause(2000);
        strcpy(message, network_manager.networks_ssids().c_str());
        P.displayReset();
        display_func = DISP_NO_WIFI_FOUND;
        break;  
      
      case DISP_CONNECTED:
        P.setTextEffect(PA_SCROLL_LEFT, PA_NO_EFFECT);
        P.setPause(2000);
        strcpy(message, "WIFI !!");
        P.displayReset();
        display_func = DISP_INIT_TIME;
        break;

      case DISP_INIT_TIME:
        display_func = DISP_DO_NOTHING;
        change_city_btn_pressed(false);
        break;
      
      case DISP_INSIDE_TEMP:
        P.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_DOWN);
        P.setPause(2000);
        weather_manager.display_inside_temp(message);
        display_func = DISP_INSIDE_HUM;
        P.displayReset();
        break;

      case DISP_INSIDE_HUM:
        P.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_DOWN);
        P.setPause(2000);
        weather_manager.display_inside_hum(message);
        display_func = DISP_TIME_NO_WEATHER;
        P.displayReset();
        break;

      case DISP_WEATHER_DESC:
        P.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_DOWN);
        P.setPause(1000);
        weather_manager.display_description(message, city_manager.city_name());
        display_func = DISP_WEATHER_TEMP;
        P.displayReset();
        break;

     case DISP_WEATHER_TEMP:
        P.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_DOWN);
        P.setPause(1000);
        weather_manager.display_temp(message);
        display_func = DISP_WEATHER_TEMP_MIN_MAX;
        P.displayReset();
        break;

     case DISP_WEATHER_TEMP_MIN_MAX:
        P.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_DOWN);
        P.setPause(1000);
        weather_manager.display_temp_min_max(message);
        display_func = DISP_WEATHER_HUM;
        P.displayReset();
        break;

      case DISP_WEATHER_HUM:
        P.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_DOWN);
        P.setPause(1000);
        weather_manager.display_hum(message);
        display_func = DISP_TIME_NO_WEATHER;
        P.displayReset();
        break;

      case DISP_TIME_NO_WEATHER:
        P.setTextEffect(PA_SCROLL_LEFT, PA_NO_EFFECT);
        P.setPause(0);
        display_format_time();
        display_func = DISP_DEFAULT;
        P.displayReset();
        break;

      case DISP_TIME:
        P.setTextEffect(PA_SCROLL_LEFT, PA_NO_EFFECT);
        P.setPause(0);
        display_format_time();
        display_func = DISP_UPDATE_WEATHER;
        P.displayReset();
        break;

      case DISP_UPDATE_WEATHER:
        btn_weather_pressed(false);
        display_func = DISP_DEFAULT;
        break;

      case DISP_DEFAULT:
        // Update time only every second
        if (millis() - lastTime >= 1000) {
          lastTime = millis();
          P.setTextEffect(PA_PRINT, PA_NO_EFFECT);
          display_format_time();
          P.displayReset();
        }
        break;

      case DISP_DO_NOTHING:
        break;
    }
  }

  // Check whether a button has been pressed
  if (btn_change_city.getSingleDebouncedRelease()) {
    change_city_btn_pressed(true);
  }

  if (btn_inside_temp.getSingleDebouncedRelease()) {
    inside_temp_hum_btn_pressed();
  }

  if (btn_weather.getSingleDebouncedRelease()) {
    btn_weather_pressed(true);
  }

  if (btn_bulb.getSingleDebouncedRelease()) {
    bulb_btn_pressed(true);
  }
}

/*
 * Transitions the currently display text outside of the screen.
 */
void display_clear() {
  P.setTextEffect(PA_PRINT, PA_SCROLL_DOWN);
  P.setPause(0);
  P.displayReset();
}

/*
 * Formats the time for display
 */
void display_format_time() {
  int h, m, s;

  h = hour();
  m = minute();
  s = second() % 2;

  sprintf(message, "%02d%s%02d", h, (s ? ":" : " ") , m);
}

/*
 * Function called when connected to Blynk Server
 * 
 * Triggers display function.
 * Updates the bulb button widget in mobile app in case they are out of sync
 * 
 */
BLYNK_CONNECTED() {
  display_clear();
  display_func = DISP_CONNECTED;
  Blynk.virtualWrite(V6, bulb.state);
}

/*
 * Receives response from time webhook
 * 
 * Extracts date/time from response and updates board's time.
 * Updates the LEDs to light up the appropriate city name.
 * Triggers display function.
 * 
 */
BLYNK_WRITE(V4) {
  String      response = param.asStr(); 
  JSONVar     data = JSON.parse(response);
  const char* _dt = data["datetime"];
  String      dt = String(_dt);

  Serial.print("Webhook Time: ");
  Serial.println(response);

  // Extract date/time parts
  int y = dt.substring(0, 0 + 4).toInt();
  int m = dt.substring(5, 5 + 2).toInt();
  int d = dt.substring(8, 8 + 2).toInt();
  int h = dt.substring(11, 11 + 2).toInt();
  int mn = dt.substring(14, 14 + 2).toInt();
  int s = dt.substring(17, 17 + 2).toInt();

  char t[20];
  sprintf(t, "%02d-%02d-%02d - %02d:%02d:%02d", y, m, d, h, mn, s);
  Serial.println(t);
  Serial.println("");

  // Update time
  setTime(h, mn, s, d, m, y);

  // Update city LEDs
  city_manager.update_led();

  // Trigger display function
  display_clear();
  display_func = DISP_TIME;
}

/*
 * Receives response from weather webhook
 * 
 * Stores the weather data in the WeatherManager.
 * Updates the LEDs to light up the appropriate weather icons.
 * Send the weather data back to the mobile application to update the corresponding widgets.
 * Triggers showing weather details on display as required.
 * 
 */
BLYNK_WRITE(V2) {
  String response = param.asStr(); 
  JSONVar data = JSON.parse(response);
  String  icon;

  Serial.print("Webhook Weather: ");
  Serial.println(response);

  // Store weather data
  weather_manager.description = data["weather"][0]["description"];
  weather_manager.description[0] = weather_manager.description[0] - 32; // Upper case
  icon = data["weather"][0]["icon"]; icon.remove(2);
  weather_manager.icon = icon.toInt(); 
  weather_manager.temp = data["main"]["temp"];
  weather_manager.temp_min = data["main"]["temp_min"];
  weather_manager.temp_max = data["main"]["temp_max"];
  weather_manager.hum = data["main"]["humidity"];

  Serial.println(weather_manager.to_String());
  Serial.println("");

  // Update LEDs
  weather_manager.update_led();

  // Update mobile app
  char s[20];
  Blynk.virtualWrite(V11, weather_manager.temp);
  Blynk.virtualWrite(V12, weather_manager.display_temp_min_max(s));
  Blynk.virtualWrite(V13, weather_manager.hum);
  Blynk.virtualWrite(V14, weather_manager.description);
  Blynk.setProperty(V15, "url", 1, weather_manager.icon_url());

  // Trigger display fuction?
  if (display_the_weather) {
    display_func = DISP_WEATHER_DESC;
    display_the_weather = false;
    display_clear();
  }
}

/*
 * "Next City" button is pressed
 * 
 * The time (V4) webhooks is triggered.
 * The mobile application is updated with the name of the city (V9).
 * 
 * change_city: Indicates if the city should be updated. Not required
 *              after a (re)connection to the Blynk Server.
 */
void change_city_btn_pressed(bool change_city) {
  if (change_city) city_manager.next_city();
  
  Blynk.virtualWrite(V4, city_manager.time_api());
  Blynk.virtualWrite(V9, city_manager.city_name());
}

/*
 * "Inside Temp/Hum" button is pressed
 * 
 * Temperature and humidity values are read from the sensor.
 * The mobile application is updated (V7, V8).
 */
void inside_temp_hum_btn_pressed() {
  weather_manager.inside_temp = dht.readTemperature();
  weather_manager.inside_hum = dht.readHumidity();

  Blynk.virtualWrite(V7, weather_manager.inside_temp);
  Blynk.virtualWrite(V8, weather_manager.inside_hum);
  
  display_clear();
  display_func = DISP_INSIDE_TEMP;
}

/*
 * "Weather" button is pressed
 * 
 * Triggers the weather webhook (V2).
 * Makes sure that the display will display the weather details.
 * 
 */
void btn_weather_pressed(bool disp_weather) {
  display_the_weather = disp_weather;
  weather_manager.all_leds_on();
  Blynk.virtualWrite(V2, city_manager.weather_api(), OPENWEATHERMAP_API_KEY);  
}

/*
 * "Toggle Bulb" button is pressed
 * 
 * Toggles the switch for the bulb.
 * The mobile application is only updated is the button is pressed on the LyoLamp.
 * 
 */
void bulb_btn_pressed(bool update_blynk) {
  bulb.toggle();
  
  if (update_blynk) {
    Blynk.virtualWrite(V6, bulb.state);
  }
}

/*
 * Mobile application buttons
 * 
 * The following functions emulate a press on one of the physical buttons from the mobile application
 * 
 */
// Next City button
BLYNK_WRITE(V5) {
  if (param.asInt() == 1) {
    change_city_btn_pressed(true);
  }
}
// Inside Temp / Hum button
BLYNK_WRITE(V10) {
  if (param.asInt() == 1) {
    inside_temp_hum_btn_pressed();
  }
}

// Weather button
BLYNK_WRITE(V1) {
  if (param.asInt() == 1) {
    btn_weather_pressed(true);
  }
}

// Toggle Bulb button
BLYNK_WRITE(V6) {
  bulb_btn_pressed(false);
}

/*
 * Timer - Updates inside temp and hum in mobile application
 * 
 * The labels (V7, V8) and the graph datastreams (V10, V21) are updated separately.
 * 
 */
void update_inside_temp_mobile() {
  Serial.println("Timer Inside Temp/Hum\n");
  
  weather_manager.inside_temp = dht.readTemperature();
  weather_manager.inside_hum = dht.readHumidity();

  
  
  // Labels
  Blynk.virtualWrite(V7, weather_manager.inside_temp);
  Blynk.virtualWrite(V8, weather_manager.inside_hum);

  // Graph
  Blynk.virtualWrite(V20, weather_manager.inside_temp);
  Blynk.virtualWrite(V21, weather_manager.inside_hum);
}

/*
 * Timer - Updates weather data in the mobile application
 * 
 * Triggers the weather webhook (V2).
 * The mobile application update is actually done in the BLYNK_WRITE(V2) function.
 * 
 */
void update_weather_mobile() {
  Serial.println("Timer Weather\n");
  Blynk.virtualWrite(V2, city_manager.weather_api(), OPENWEATHERMAP_API_KEY);
}
