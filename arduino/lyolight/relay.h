#ifndef _RELAY_H_
#define _RELAY_H_

/*
 * Support class to manage a simple relay
 * 
 * contron_pin: Pin used to control the relay
 * state:       Current state of the relay (HiGH or LOW)
 * 
 * begin():     Initiates the relay. Default state is LOW (OFF)
 * toggle():    Toggles the relay ON/OFF
 * 
 */
class Relay {
  public:
  Relay(int _control_pin) {
    control_pin = _control_pin;
  }

  int control_pin;
  int state;

  void begin() {
    state = LOW;
    pinMode(control_pin, OUTPUT);
    digitalWrite(control_pin, state);
  }
  
  void toggle() {
    state = (state == HIGH) ? LOW : HIGH;
    digitalWrite(control_pin, state);
  } 
};

#endif
