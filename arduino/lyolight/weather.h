#ifndef _WEATHER_H_
#define _WEATHER_H_

#include "shift_register.h"

/*
 * LED idendifier in the shift register for each weather icon.
 * Weather icons use the 4 bits on the left of the register (least significant bits).
 * OpenWeatherMap has more icons but we have simplified things and combine them to achieve a similar result.
 * OpenWeatherMap icon: https://openweathermap.org/weather-conditions
 * 
 */
#define WEATHER_SUN   0x01
#define WEATHER_CLOUD 0x02
#define WEATHER_RAIN  0x04
#define WEATHER_FLAKE 0x08
#define WEATHER_ALL   0x0F

/*
 * Weather icon combinations allowing to light up several icons at the time.
 */
#define WEATHER_CLEAR           WEATHER_SUN
#define WEATHER_PARTIAL_CLOUDS  WEATHER_SUN | WEATHER_CLOUD
#define WEATHER_CLOUDS          WEATHER_CLOUD
#define WEATHER_LIGHT_RAIN      WEATHER_RAIN | WEATHER_SUN
#define WEATHER_SHOWER_RAIN     WEATHER_RAIN | WEATHER_CLOUD
#define WEATHER_SNOW            WEATHER_FLAKE

/*
 * URL of icons to be displayed in the mobile application.
 * This can be changed to link to a different set of icons.
 */
#define WEATHER_ICON_BASE_URL "https://gitlab.com/gdeflaux/lyolight/raw/master/blynk-app/weather-icons/export/"
#define WEATHER_ICON_EXT      "png"

/*
 * Support class to manager weather data and operations
 * 
 * description:             Short description of the weather returned by OpenWeatherMap
 * icon:                    Weather icon returned by OpenWeatherMap
 * temp:                    Temperature returned by OpenWeatherMap
 * temp_min:                Min temperature returned by OpenWeatherMap
 * temp_max:                Max temperature returned by OpenWeatherMap
 * hum:                     Humidity returned by OpenWeatherMap
 * inside_temp:             Temperature returned by the internal sensor
 * inside_hum:              Humidity returned by the internal sensor
 * sr:                      Shift register
 * 
 * to_String():             Debugging function that displays all weather data in a readable format
 * update_led():            Lights up the appropriate LEDs based on the icon returned by OpenWeatherMap
 * all_leds_on():           Turns all weather icon LEDs on, indicating that we are awaiting for the webhook response
 * display_description():   Formats 'description' for the display
 * display_temp():          Formats 'temp' for the display
 * display_temp_min_max():  Formats the temperature range for the display
 * display_hum():           Formats 'hum' for the display
 * display_inside_temp():   Formats 'inside_temp' for the display
 * display_inside_hum():    Formats 'inside_hum' for the display
 * icon_url():              Returns the URL for the corresponding icon (mobile application)
 * 
 */
class WeatherManager {
  public:
  WeatherManager(ShiftRegister *_sr) {
    description = "No data";
    icon = 99;
    temp = 99;
    temp_min = 99;
    temp_max = 99;
    hum = 99;
    inside_temp = 99;
    inside_hum = 99;
    sr = _sr;
  }
  
  String  description;
  int     icon;
  double  temp;
  double  temp_min;
  double  temp_max;
  double  hum;
  double  inside_temp;
  double  inside_hum;
  ShiftRegister *sr;

  String to_String() {
    return String("Desc: " + description + "\n" +
                  "Icon: " + icon + "\n" +
                  "Temp: " + temp + " (" + temp_min + " - " + temp_max + ")\n" +
                  "Hum:  " + hum);
  }

  void update_led() {
    // Icon numbers correspond to OpenWeatherMap icons.
    switch (icon) {
      case 1: // Sun
        sr->update_register(WEATHER_CLEAR, SR_RIGHT_SIDE);
        break;

      case 2: // Partial clouds
        sr->update_register(WEATHER_PARTIAL_CLOUDS, SR_RIGHT_SIDE);
        break;
       
      case 3: // Clouds
      case 4:
      case 50:
        sr->update_register(WEATHER_CLOUDS, SR_RIGHT_SIDE);
        break;
        
      case 9: // Shower Rain
      case 11:
        sr->update_register(WEATHER_SHOWER_RAIN, SR_RIGHT_SIDE);
        break;

      case 10: // Light rain
        sr->update_register(WEATHER_LIGHT_RAIN, SR_RIGHT_SIDE);
        break;
      
      case 13: // Snow
        sr->update_register(WEATHER_SNOW, SR_RIGHT_SIDE);
        break; 
    }
  }

  void all_leds_on() {
    sr->update_register(WEATHER_ALL, SR_RIGHT_SIDE);
  }

  void display_description(char *message, String city) {
    String m("Weather in " + city + " - " + description);
    strcpy(message, m.c_str());
  }

  void display_temp(char *message) {
    String m(temp, 1);
    m += "C";
    strcpy(message, m.c_str());
  }

  char *display_temp_min_max(char *message) {    
    String t_min(temp_min, 1);
    String t_max(temp_max, 1);

    sprintf(message, "%sC-%sC", t_min.c_str(), t_max.c_str());
    
    return message;
  }

  void display_hum(char *message) {
    String h(hum, 1);
    h += "%";
    strcpy(message, h.c_str());
  }

  void display_inside_temp(char *message) {
    String m(inside_temp, 1);
    m += "C";
    strcpy(message, m.c_str());
  }

  void display_inside_hum(char *message) {
    String h(inside_hum, 1);
    h += "%";
    strcpy(message, h.c_str());
  }

  String icon_url() {
    String s(WEATHER_ICON_BASE_URL);
    s += icon;
    s += ".";
    s += WEATHER_ICON_EXT;
    return s;
  }
  
};

#endif
