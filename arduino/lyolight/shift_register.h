#ifndef _SHIFT_REGISTER_H_
#define _SHIFT_REGISTER_H_

#define SR_LEFT_SIDE  0
#define SR_RIGHT_SIDE  1

/*
 * Support class to manage the Shift Register
 * 
 * latch_pin:         Latch pin on the shift register
 * clock_pin:         Clock pin on the shift register
 * data_pin:          Data pin on the shift register
 * reg:               Last (current) value writen to the register
 * 
 * begin():           Initiates the shift register's pins
 * update_register(): Writes a byte ('new_byte') to the register. 
 *                    The 'side' argument specificies which of the first or last 4 bits must be updated.
 *                    4 bits are used for cities, 4 bits are used for weather icons.
 * 
 */
class ShiftRegister {
  public:
  ShiftRegister(int _latch_pin, int _clock_pin, int _data_pin) {
    latch_pin = _latch_pin;
    clock_pin = _clock_pin;
    data_pin = _data_pin;
    reg = 0;
  }

  int   latch_pin;
  int   clock_pin;
  int   data_pin;
  byte  reg;

  void begin() {
    pinMode(latch_pin, OUTPUT);
    pinMode(clock_pin, OUTPUT);  
    pinMode(data_pin, OUTPUT);
  }

  void update_register(byte new_byte, int side) {
    byte b;

    if (side == SR_RIGHT_SIDE) { // Updating LEDs for the weather icons
      b = reg & 0xF0; // The bits on the right (least significant) are cleared, while the bits on the left (most significant) are unchanged
      b = b | new_byte;
    } else { // Updating LEDs for a city
      b = reg & 0x0F; // The bits on the left (most significant) are cleared, while the bits on the right (least significant) are unchanged
      b = b | new_byte;
    }

    // The byte 'b' is written to the register
    digitalWrite(latch_pin, LOW);
    shiftOut(data_pin, clock_pin, LSBFIRST, b);
    digitalWrite(latch_pin, HIGH);

    reg = b;
  }
};

#endif
