#ifndef _WIFI_NETWORK_H_
#define _WIFI_NETWORK_H_

#define NETWORK_NOT_FOUND -1

/*
 * Data definition of wifi networks
 * 
 * ssid:      SSID of the wifi network
 * password:  Password of the wifi network
 * strength:  Strenght of the signal of the wifi network
 * wifi_id:   ID of network returned by WiFiNINA
 */
class WifiNetwork {
  public:

  WifiNetwork (String _ssid, String _password) {
    ssid = _ssid;
    password = _password;
    wifi_id = NETWORK_NOT_FOUND;  
  }

  String  ssid;
  String  password;
  long    strength;
  int     wifi_id;
};

/*
 * Support functions to manage wifi networks data and operations
 * 
 * networks:                Array of WifiNetwork objects
 * n_networks:              Number of wifi networks in the 'networks' attribute
 * strongest_network_id:    Array ID of the network with the strongest signal
 * 
 * scanNetworks():          Scans the visible networks for the known networks defined in 'private_keys_vars.h'.
 *                          If a know network is found, the strength of the signal is recorder.
 *                          Returns 'true' if a know network is found, 'false' otherwise.
 * find_network_by_ssid():  Checks if a visible network is known.
 *                          Returns the ID of the know network if found, NETWORK_NOT_FOUND otherwise.
 * best_network():          Returns the wifi network with the strongest signal (the one Lyolight will try to connect to).
 * networks_ssids():        Returns a '|' separated string with all known wifi network names.
 */
class WifiNetworkManager {
  public:
  WifiNetworkManager (int _n_networks, WifiNetwork *_networks) {
    n_networks = _n_networks;
    networks = _networks;
  }

  WifiNetwork *networks;
  int         n_networks;
  int         strongest_network_id;

  bool scanNetworks() {
    int   n_visible = WiFi.scanNetworks();
    long  best_strength = -100000;
    bool  found = false;

    if (n_visible == 0) {
      Serial.println("No networks found are visible");
    } else {
      for (int i = 0; i < n_visible; i++) {
        int n_id = find_network_by_ssid(WiFi.SSID(i));
        if (n_id != NETWORK_NOT_FOUND) {
          found = true;
          
          networks[n_id].wifi_id = i;
          networks[n_id].strength = WiFi.RSSI(i);
          if (networks[n_id].strength > best_strength) {
            strongest_network_id = n_id;
            best_strength = networks[n_id].strength;
          }
          
          Serial.print("Found: ");
          Serial.print(networks[n_id].ssid + " - ");
          Serial.println(networks[n_id].strength);
          Serial.println("");
        }
      }
      if (found) {
        Serial.println("Strongest: " + networks[strongest_network_id].ssid);
      } else {
        Serial.println("No know network found");
      }
    }

    return found;
  }

  int find_network_by_ssid(String ssid) {
    for (int i = 0; i < n_networks; i++) {
      if (networks[i].ssid == ssid) return i;
    }
  
    return NETWORK_NOT_FOUND;
  }

  WifiNetwork *best_network() {
    return &networks[strongest_network_id];
  }

  String networks_ssids() {
    String s = "";

    for (int i = 0; i < n_networks; i++) {
      s += networks[i].ssid;
      if (i != n_networks - 1)  s += " | ";
    }

    s = "Known networks: " + s;
    return s;
  }
};

#endif
